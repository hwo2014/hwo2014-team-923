#include "game_logic.h"
#include "protocol.h"
#include <math.h>
#include <numeric>

using namespace hwo_protocol;

namespace {
    struct CarPosition {
        CarPosition operator()(jsoncons::json const& car_json) {
            CarPosition result;

            result.id.name  = car_json["id"]["name"].as<std::string>();
            result.id.color = car_json["id"]["color"].as<std::string>();

            result.angle = car_json["angle"].as<double>();

            result.piecePosition.pieceIndex       = car_json["piecePosition"]["pieceIndex"].as<size_t>();
            result.piecePosition.inPieceDistance  = car_json["piecePosition"]["inPieceDistance"].as<double>();
            result.piecePosition.lane.startLaneIndex = car_json["piecePosition"]["lane"]["startLaneIndex"].as<unsigned>();
            result.piecePosition.lane.endLaneIndex   = car_json["piecePosition"]["lane"]["endLaneIndex"].as<unsigned>();
            result.piecePosition.lap                 = car_json["piecePosition"]["lap"].as<unsigned>();

            return result;
        }
        struct {std::string name;std::string color;} id;
        double angle;
        struct {
            unsigned pieceIndex;
            double inPieceDistance;
            struct {unsigned startLaneIndex;unsigned endLaneIndex;}lane;
            int lap;
        } piecePosition;
    };
    typedef std::vector<CarPosition> CarsPositions;
}

game_logic::game_logic()
  : action_map(fill_action_map())
  , throttlePid(2.0,0.0,10.0)
  , angle(0)
  , jsonLog("traffic.json")
  , jsonFirst(true)
{
    distance = prevPiecesDistance = inPieceDistance = speed = speed = 0.0;
    throttle = 0.5;
    mu_g = 36./100.;
    estimations_were_done = false;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  if (jsonLog.is_open()) {
    if (jsonFirst) {
      jsonLog << "[" << std::endl;
      jsonFirst = false;
    } else
      jsonLog << "," << std::endl;
    jsonLog << jsoncons::pretty_print(msg);
    if (msg_type=="tournamentEnd")
      jsonLog << "]" << std::endl;
  }
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return game_logic::msg_vector(1, make_ping());
  }
}

void game_logic::log_telemetry(const jsoncons::json& telemetry) {
  if (jsonLog.is_open()) {
    jsoncons::json msg;
    msg["msgType"] = "telemetry";
    msg["data"] = telemetry;
    jsonLog << "," << jsoncons::pretty_print(msg);
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  return game_logic::msg_vector(1, make_ping());
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  ourName = data["name"].as<std::string>();
  ourColor = data["color"].as<std::string>();
  std::cout << "Joined as " << ourName << " " << ourColor << std::endl;
  return game_logic::msg_vector(1, make_ping());
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return game_logic::msg_vector(1, make_ping());
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  auto race = data["race"];
  const jsoncons::json& track = race["track"];
  const jsoncons::json& tpieces = track["pieces"];

  const jsoncons::json& tlanes = track["lanes"];

  lanes.resize(tlanes.size());
  std::for_each(tlanes.begin_elements(), tlanes.end_elements(), 
      [this](jsoncons::json const& jsn){
          this->lanes[jsn["index"].as_uint()].dist_from_center = jsn["distanceFromCenter"].as_double();
          this->lanes[jsn["index"].as_uint()].index = jsn["index"].as_uint();
      }
  );
  

  for (auto it = tpieces.begin_elements(); it != tpieces.end_elements(); ++it) {
    Piece piece;
    piece.Length.resize(lanes.size());
    piece.Radius.resize(lanes.size());
    piece.MaxAlowedSpeed.resize(lanes.size());
    std::cout << jsoncons::pretty_print(*it) << std::endl;
    if (it->has_member("angle")) {
        piece.Straight = false;
        piece.Angle    = (*it)["angle"].as<double>();
        for (size_t i = 0; i < lanes.size(); ++i) {
            piece.Radius[i]   = (*it)["radius"].as<double>() + lanes[i].dist_from_center;
            piece.Length[i]   = (piece.Angle*3.1412926/180) * piece.Radius[i];
            piece.MaxAlowedSpeed[i] = 10;
        };
    } else {
        piece.Straight = true;
        piece.Angle    = 0;
        for (size_t i = 0; i < lanes.size(); ++i) {
            piece.Radius[i]   = 1000*1000*1000; // Radius of straight line is really high
            piece.Length[i]   = (*it)["length"].as<double>();
            piece.MaxAlowedSpeed[i] = 10;
        };
    }
    piece.Switch = (it->has_member("switch")) && (*it)["switch"].as<bool>();

    std::cout << "Piece: " << piece.Length[0] << " " << piece.Angle << " " << piece.Switch << std::endl;
    pieces.push_back(piece);
    
  }
  return game_logic::msg_vector(1, make_ping());
}


game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    if (pieces.size() == 0) {
        return game_logic::msg_vector(1, make_ping());
    }

    CarsPositions cars_positions;
    std::transform(data.begin_elements(), data.end_elements(), std::back_inserter(cars_positions), CarPosition());
    CarPosition const& our_car = *std::find_if(cars_positions.cbegin(), cars_positions.cend(), [this](CarPosition const& cp){return cp.id.color == this->ourColor;});


    // bot code here
    double const prev_angle = angle;
    angle      = our_car.angle;
    size_t const piece_id = our_car.piecePosition.pieceIndex;
    size_t const next_piece_id = (piece_id < pieces.size() - 1) ? piece_id + 1 : 0;
    size_t const lane = our_car.piecePosition.lane.startLaneIndex;
    speed = (our_car.piecePosition.inPieceDistance > inPieceDistance
        ? our_car.piecePosition.inPieceDistance - inPieceDistance
        : ((our_car.piecePosition.inPieceDistance - inPieceDistance) > 0.01)
        ? 0
        : speed
        );
    distance += speed;
    inPieceDistance = our_car.piecePosition.inPieceDistance;
    double const dist_to_next_piece = pieces[piece_id].Length[lane] - inPieceDistance;

    // parameters to estimate:
    //throttle = 0.4;
    if (!estimations_were_done && our_car.piecePosition.lap == 0) {
        static enum {E_ACCELERATION, E_BEND_MG, E_DONE} estimation = E_BEND_MG;

        switch (estimation)
        {
        case E_ACCELERATION: {
            static double braking_speed_start = 0;
            static double braking_speed_end   = 0;
            static double braking_start_distance    = distance;
                 
            static bool braking = false;
            if (speed >= 3.0 && !braking) {
                throttle = 0.0;
                braking  = true;
                braking_speed_start = speed;
                braking_start_distance = distance;
            }
            if (speed <= 0.2 && braking) {
                braking  = false;
                braking_speed_end = speed;
                double const appox_accel = (-braking_speed_start*braking_speed_start + braking_speed_end*braking_speed_end)/(2*(distance-braking_start_distance));
                std::cout
                    << "braking_speed_start: " << std::setprecision(5) << braking_speed_start << std::endl
                    << "braking_speed_end: "   << braking_speed_end << std::endl
                    << "distance: "   << (distance-braking_start_distance) << std::endl
                    << "Approx acceleration: " << appox_accel << std::endl << std::setprecision(2);
                estimation = E_DONE;
            }
            }
            break;
        case E_BEND_MG:
            if (pieces[piece_id].Angle > 0) {
                if (fabs(angle) < 0.1) {
                    throttle += 0.01;
                }
                else {
                    throttle = 0.0;
                    mu_g = speed*speed/pieces[piece_id].Radius[lane];
                    std::for_each(pieces.begin(), pieces.end(), [this](Piece & p){
                        for (size_t i = 0; i < lanes.size(); ++i)
                        {
                            p.MaxAlowedSpeed[i] = 0.99 * sqrt(fabs(this->mu_g*p.Radius[i]));
                        }
                    });
                    estimation = E_ACCELERATION;
                    estimation = E_DONE;
                    std::cout << "Estimation of BEND_MG was done: " << mu_g << std::endl;
                }
            }
            else {
                throttle = 0.45;
            }
            break;
        case E_DONE:
        default:
            estimations_were_done = true;
            break;
        }

        if (E_DONE == estimation) {
            // Fill MaxSpeed for Pieces]
            typedef std::map<double, double> speed_profile;
            std::vector<speed_profile> lanes_ideal_profiles;
            speed_profile ideal_speed_profile;

        }
    }
    else {
        bool const acceptable_angle1 = fabs(our_car.angle) < 50;
        bool const acceptable_angle = fabs(prev_angle) >= fabs(our_car.angle);
        bool const straight_line = (pieces[piece_id].Straight && pieces[next_piece_id].Straight);
        bool const speed_too_high =
            pieces[piece_id].Straight && !pieces[next_piece_id].Straight &&
            //dist_to_next_piece < 30 && // we need to estimate this constant precisely
            speed > 0.9*pieces[next_piece_id].MaxAlowedSpeed[lane];

        double const recommended_speed = pieces[piece_id].MaxAlowedSpeed[lane];
        bool const low_speed = speed < recommended_speed && !pieces[our_car.piecePosition.pieceIndex].Straight;

        static bool stop = false;

        if (/*!speed_too_high && */acceptable_angle1 && (acceptable_angle || straight_line || low_speed)) {
            if (pieces[our_car.piecePosition.pieceIndex].Straight) {
                throttle = 1.0;
            }
            else {
                throttle = pieces[next_piece_id].MaxAlowedSpeed[lane]*10;
            }
        }
        else {
            if (fabs(recommended_speed-speed)>0.5) {
                throttle = 0.;
            }
            else {
                throttle = 0.1;
            }
        }
        //std::cout << (speed_too_high? "speed is too high" : "speed is OK") << std::endl;
    }


    throttle = throttle < 0 ? 0 : (throttle > 1.0 ? 1.0 : throttle);
    
    //
    auto print_to_stream = [&](std::ostream &out, char const delim){
        out
            << /*1*/ our_car.piecePosition.lap << "/2" << delim
            << /*2*/ our_car.piecePosition.pieceIndex << "/" << pieces.size()-1 << delim
            << /*3*/ our_car.piecePosition.inPieceDistance << delim
            //<< /**/ pieces[our_car.piecePosition.pieceIndex].Length - our_car.piecePosition.inPieceDistance << ", "
            << /*4*/ pieces[our_car.piecePosition.pieceIndex].Radius[lane] << delim
            << /*5*/ throttle<< delim
            << /*6*/ our_car.angle<< delim
            << /*7*/ speed<< delim
            << /*8*/ pieces[piece_id].MaxAlowedSpeed[lane]
            << std::endl;
    };
    jsoncons::json telemetry;
    telemetry["throttle"] = throttle;
    log_telemetry(telemetry);
    static std::ofstream log("germany_7.csv");
    std::cout.precision(2); std::cout << std::fixed;
    log.precision(10);
    print_to_stream(std::cout, '\t');
    print_to_stream(log, ',');


    std::string do_switch;
    if (do_switch != "")
        return game_logic::msg_vector(1, make_switch(do_switch));
    else
        return game_logic::msg_vector(1, make_throttle(throttle));
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return game_logic::msg_vector(1, make_ping());
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return game_logic::msg_vector(1, make_ping());
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return game_logic::msg_vector(1, make_ping());
}

std::map<std::string, game_logic::action_fun> game_logic::fill_action_map()
{
    std::map<std::string, game_logic::action_fun> result;

    result["join"] = std::bind(&game_logic::on_join, std::placeholders::_1, std::placeholders::_2);
    result["yourCar"] = std::bind(&game_logic::on_your_car, std::placeholders::_1, std::placeholders::_2);
    result["gameInit"] = std::bind(&game_logic::on_game_init, std::placeholders::_1, std::placeholders::_2);
    result["gameStart"] = std::bind(&game_logic::on_game_start, std::placeholders::_1, std::placeholders::_2);
    result["lapFinished"] = std::bind(&game_logic::on_lap_finished, std::placeholders::_1, std::placeholders::_2);
    result["carPositions"] = std::bind(&game_logic::on_car_positions, std::placeholders::_1, std::placeholders::_2);
    result["crash"] = std::bind(&game_logic::on_crash, std::placeholders::_1, std::placeholders::_2);
    result["gameEnd"] = std::bind(&game_logic::on_game_end, std::placeholders::_1, std::placeholders::_2);
    result["error"] = std::bind(&game_logic::on_error, std::placeholders::_1, std::placeholders::_2);

    //result.insert(std::make_pair("yourCar", &game_logic::on_your_car));
    //result.insert(std::make_pair("gameInit", &game_logic::on_game_init));
    //result.insert(std::make_pair("gameStart", &game_logic::on_game_start));
    //result.insert(std::make_pair("carPositions", &game_logic::on_car_positions));
    //result.insert(std::make_pair("crash", &game_logic::on_crash));
    //result.insert(std::make_pair("gameEnd", &game_logic::on_game_end));
    //result.insert(std::make_pair("error", &game_logic::on_error));
    return result;
}

game_logic::msg_vector game_logic::on_lap_finished( const jsoncons::json& data )
{
    std::cout << "Lap finished: " << data["car"]["color"].to_string() << std::endl;
    std::cout << "Lap time:     " << data["lapTime"]["millis"].as_uint()/1000.0 << std::endl;
    return game_logic::msg_vector(1, make_ping());
}
