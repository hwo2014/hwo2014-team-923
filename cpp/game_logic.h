#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "pid.h"


struct Lane {
    double dist_from_center;
    size_t index;
};

struct Piece {
    std::vector<double> Length; // Here is vector because of each lane has different length
    std::vector<double> Radius;
    double Angle;
    bool Straight;
    bool Switch;
    std::vector<double> MaxAlowedSpeed;
};

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  typedef std::map<std::string, action_fun> action_map_t;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:

  const std::map<std::string, action_fun> action_map;

  std::vector<Lane> lanes;
  std::vector<Piece> pieces;
  SimplePid throttlePid;
  std::string ourName;
  std::string ourColor;
  std::ofstream jsonLog;
  bool jsonFirst;
  double angle;
  double distance;
  double speed;
  double throttle;
  double prevPiecesDistance;
  double inPieceDistance;

  double mu_g;
  bool estimations_were_done;

  std::map<std::string, action_fun> fill_action_map();
  void log_telemetry(const jsoncons::json& telemetry);
  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_lap_finished(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
};

#endif
