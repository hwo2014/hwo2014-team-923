#include "pid.h"

SimplePid::SimplePid(double P, double I, double D, double differentiator, double integrator, double integrator_max, double integrator_min) {
  this->P = P;
  this->I = I;
  this->D = D;
  this->differentiator = differentiator;
  this->integrator = integrator;
  this->IntegratorMax = integrator_max;
  this->IntegratorMin = integrator_min;
  setPoint = 0;
}

double SimplePid::Update(double current_value) {
  double error = setPoint - current_value;
  double P_value = P * error;
  double D_value = D * ( error - differentiator );
  differentiator = error;
  integrator += error;
  if (integrator > IntegratorMax)
    integrator = IntegratorMax;
  else if (integrator < IntegratorMin)
    integrator = IntegratorMin;
  double I_value = I * integrator;
  double PID = P_value + I_value + D_value;
  return PID;
}

void SimplePid::SetPoint(double set_point) {
  setPoint = set_point;
  integrator = differentiator = 0;
}
