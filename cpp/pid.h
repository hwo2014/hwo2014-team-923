#ifndef HWO_PID_H
#define HWO_PID_H

class SimplePid
{
public:
  double IntegratorMax;
  double IntegratorMin;
  double P,I,D;

  SimplePid(double P=2.0, double I=0.0, double D=1.0, double differentiator=0, double integrator=0, double integrator_max=500, double integrator_min=-500);
  double Update(double current_value);
  void SetPoint(double set_point);

private:
  double differentiator;
  double integrator;
  double setPoint;
};

#endif
