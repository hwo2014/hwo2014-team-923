#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    jsoncons::json botId;
    botId["name"] = name;
    botId["key"] = key;
    data["botId"] = botId;
    data["trackName"] = "keimola";//keimola germany
    data["password"] = "qwerty123456";
    data["carCount"] = 1;
    return make_request("joinRace", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

  jsoncons::json make_switch(const std::string& direction)
  {
    return make_request("switchLane", direction);
  }
}  // namespace hwo_protocol
